/**
 *  20190818 Sun
 *  arpspoof - arpspoof.h
 *  1. send arp request to target ip(from argument) 
 *  2. receive the arp response 
 *  3. and then send malicious arp reply to target host
 *
 **/


#pragma once
#include <pcap.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// to get the MAC Address & IP address
#include <sys/ioctl.h>
#include <net/if.h>

#include <arpa/inet.h> 
#include <sys/types.h> 
#include <sys/socket.h>

#pragma pack(push, 1)

#define MAC_LEN 6
#define IP_ADDR_LEN 4
#define ARP_ETH_TYPE 0x0806
#define IPV4_PROTO 0x0800

#define ARP_HDR_LEN 28
#define ETHERNET_HDR_LEN 14

// ethernet header : 14B
// 1. smac, dmac
// 2. type field : network layer - ip protocol or not?
struct ethernet_header{
    u_int8_t dmac[MAC_LEN];
    u_int8_t smac[MAC_LEN];
    u_int16_t type;
};

// arp packet format struct
// for extracting mac address from arp packet
struct arp{
    u_int16_t hw_type;
    u_int16_t proto_type;
    u_int8_t hw_len; 		//length of mac address, Byte, depend on hardware type.
    u_int8_t proto_len;		//length of protocol address, Byte, depend on protocol type. 
    u_int16_t opcode;		
    u_int8_t sdr_mac[MAC_LEN];
    u_int32_t sdr_ip;
    u_int8_t tgt_mac[MAC_LEN];
    u_int32_t tgt_ip;
};

// ip header
// 1. sip, dip
// 2. ihl field : length of ip header - tcp starting offset?
struct ip_header{
    u_int8_t ver_ipHdrLen;           // 0xf0 & ver_ipHdrLen = ver, 0x0f & ver_ipHdrLen = ipHdrLen
    u_int8_t service;
    u_int16_t total_len;
    u_int16_t id;
    u_int16_t flags_fragOffset;
    u_int8_t ttl;
    u_int8_t proto;
    u_int16_t checksum;
    u_int32_t sip;
    u_int32_t dip;
    char *options;
};

struct arp_packet{
    struct ethernet_header ethernet;
    struct arp		   arp;
};

struct ip_packet{
    struct ethernet_header ethernet;
    struct ip_header	   ipHdr;
};

struct session{
    u_int8_t sdr_mac[MAC_LEN];
    u_int32_t sdr_ip;
    u_int8_t tgt_mac[MAC_LEN];
    u_int32_t tgt_ip; 
};


/**
 * enum hw type of arp packet
 * https://darksoulstory.tistory.com/59
 */
enum {
	RESERVED 		= 0,
	ETHERNET 		= 1,
	EXPERIMENTAL_ETHERNET 	= 2,
	AMATEUR_RADIO_AX_25 	= 3,
	PROTEON_PRONET_TOKEN_RING = 4,
	CHAOS			= 5,
	IEEE_802_NETWORKS	= 6,
	ARCNET			= 7,
	HYPERCHANNEL		= 8,
	LANSTAR 		= 9,
	AUTONET_SHORT_ADDRESS	= 10,
	LOCALTALK		= 11,
	LOCALNET 		= 12,
	ULTRA_LINK		= 13,	
	SMDS			= 14,
	FRAME_RELAY		= 15,
	ATM			= 16,
	HDLC			= 17,
	FIBRE_CHANNEL		= 18,
	SERIAL_LINE 		= 20,
	ARPSEC 			= 30,
	IPSEC_TUNNEL		= 31,
};

#if 0

/**
 * enum protocol type of arp packet
 * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xml
 */
enum {
	HOPOPT	 		= 0,
	ICMP	 		= 1,
	IGMP		 	= 2,
	GGP		 	= 3,
	IPv4			= 4,
	ST			= 5,
	TCP			= 6,
	CBT			= 7,
	EGP			= 8,
	IGP	 		= 9,
	BBN_RCC_MON		= 10,
	NVP_2			= 11,
	PUP	 		= 12,
	ARGUS			= 13,	
	EMCON			= 14,
	XNET			= 15,
};

#endif

/**
 * enum opcode of arp packet
 * https://www.iana.org/assignments/arp-parameters/arp-parameters.xhtml
 */
enum {
	ARP_RESERVED		= 0,
	ARP_REQUEST		= 1,
	ARP_REPLY		= 2,
	RARP_REQUEST		= 3,
	RARP_RESPONSE		= 4,
	DRARP_REQUEST 		= 5,
	DRARP_REPLY		= 6,
	DRARP_ERROR		= 7,
	INARP_REQUEST		= 8,
	INARP_RESPONSE		= 9,
	ARP_NAK			= 10,
};

void usage();

int get_host_mac(ifreq *h_IfReq, char *nic_name);
void get_host_ip(struct ifreq* ifr, char *nic_name);

void set_arp_req(arp_packet* arp_p, ifreq* ifrq, const u_char* attcker_mac, u_int32_t tgt_ip);
void set_arp_reply(arp_packet* dest_arp_pkt, arp_packet* rcvd_arp_pkt, u_int32_t tgt_ip);

void print_arp_packet_dump(arp_packet* arp_p);
void print_ip_packet_dump(ip_packet* ip_p);

bool check_arp_reply(arp_packet* arp_pkt_dump, u_int32_t sdr_ip, u_int8_t* h_mac);
bool check_arp_request(arp_packet* arp_pkt_dump, u_int32_t sdr_ip, u_int32_t tgt_ip);

#if 0
bool check_relay(ip_packet* ip_p, u_int32_t sender_ip, u_int8_t* att_mac);
#endif

void relay(pcap_t* h, ip_packet* ip_p, u_int32_t pkt_size, u_int8_t* target_mac, u_int8_t* attacker_mac);
