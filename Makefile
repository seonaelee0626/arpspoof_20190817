all : arpspoof

arpspoof: arpspoof.o main.o
	g++ -g -o arpspoof arpspoof.o main.o -lpcap

arpspoof.o: arpspoof.h arpspoof.cpp 
	g++ -g -c -o arpspoof.o arpspoof.cpp

main.o: arpspoof.h main.cpp 
	g++ -g -c -o main.o main.cpp 
clean:
	rm -f arpspoof
	rm -f *.o

